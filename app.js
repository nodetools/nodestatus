(function () {
    "use strict";
    /* eslint-env node */

    var app = require("express")(),
        server;


    function onServerReady() {
        var currentHost = server.address().address;
        var currentPort = server.address().port;
        console.log("Example app listening at http://%s:%s", currentHost, currentPort);
    }

    function initServer(port) {
        app.get("/", function (req, res) {
            res.send("Hello Cubie");
        });
        server = app.listen(port, onServerReady);
    }

    function run(port) {
        initServer(port);
    }

    return {
        run: run
    };

}().run(8001));
